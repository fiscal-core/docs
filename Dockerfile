FROM alpine:3.19.1

RUN apk add --update --no-cache unzip ca-certificates openssl ncftp wget && \
    update-ca-certificates